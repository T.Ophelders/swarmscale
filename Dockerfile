FROM alpine:3.13

RUN apk add --update --no-cache docker

COPY main /etc/go/swarmscale
RUN which docker

CMD ["/etc/go/swarmscale"]
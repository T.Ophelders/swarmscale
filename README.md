Usage notes:

Put the following labels in the containers you wish to be scaled:

- swarm_swarmscale: Bool | Toggle swarmscale: true to autoscale, false to ignore
- swarm_swarmscale_maximum: Int or 'nodes' | Maximum number of instances to run. Use 'nodes' for number of nodes in the cluster.
- swarm_swarmscale_minimum: Int | Minimum number of instances to run.

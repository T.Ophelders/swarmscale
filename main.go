package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"time"
)

var (
	CPU_PERCENTAGE_UPPER_LIMIT = 85
	CPU_PERCENTAGE_LOWER_LIMIT = 25
	PROMETHEUS_URL = os.Getenv("PROMETHEUS_URL")
	PROMETHEUS_API = "api/v1/query?query="
	PROMETHEUS_QUERY = "sum(rate(container_cpu_usage_seconds_total%7Bcontainer_label_com_docker_swarm_task_name%3D~%27.%2B%27%7D%5B5m%5D))BY(container_label_com_docker_swarm_service_name%2Cinstance)*100"
)

type prometheusPayload struct {
	Status string `json:"status"`
	Data prometheusData `json:"data"`
}

type prometheusData struct {
	ResultType string `json:"resultType"`
	Result []prometheusResult `json:"result"`
}

type prometheusResult struct {
	Metric prometheusMetric `json:"metric"`
	Value  []string    `json:"value"`
}

type prometheusMetric struct {
	Container_label_com_docker_swarm_service_name string `json:"container_label_com_docker_swarm_service_name"`
	Instance                                      string `json:"instance"`
}

type dockerServiceInspectOut struct {
	ID string `json:"ID"`
	Spec dockerServiceInspectOutSpec `json:"Spec"`
}

type dockerServiceInspectOutSpec struct {
	Name string `json:"Name"`
	Labels dockerServiceInspectOutSpecLabel `json:"Labels"`
	Mode dockerServiceInspectOutMode `json:"Mode"`
}

type dockerServiceInspectOutMode struct {
	Replicated dockerServiceInspectOutReplicated `json:"Replicated"`
}

type dockerServiceInspectOutReplicated struct {
	Replicas int `json:"Replicas"`
}

type dockerServiceInspectOutSpecLabel struct {
	Swarm_swarmscale         string `json:"swarm_swarmscale"`
	Swarm_swarmscale_maximum string `json:"swarm_swarmscale_maximum"`
	Swarm_swarmscale_minimum string `json:"swarm_swarmscale_minimum"`
}

func main() {
	for {
		fmt.Println("Starting swarmscale checks...")
		res, err := http.Get(fmt.Sprintf("%s/%s%s", PROMETHEUS_URL, PROMETHEUS_API, PROMETHEUS_QUERY))
		if err != nil {
			fmt.Println(err)
		} else {
			if res.StatusCode == http.StatusOK {
				bodyBytes, err := ioutil.ReadAll(res.Body)

				if err != nil {
					log.Fatal(err)
				}

				var prometheusPld prometheusPayload
				err = json.Unmarshal(bodyBytes, &prometheusPld)

				if err != nil {
					fmt.Println(err)
				}

				nodeListCmd := exec.Command("/bin/sh", "-c", "/usr/bin/docker node ls -q | wc -l")
				nodeCount, err := nodeListCmd.Output()

				if err != nil {
					panic(err)
				}

				fmt.Println(fmt.Sprintf("Active nodes in cluster: %s", nodeCount))

				for _, promResult := range prometheusPld.Data.Result {
					fmt.Println()
					dockerServiceCmd := exec.Command("/bin/sh", "-c", fmt.Sprintf("docker service inspect %s", promResult.Metric.Container_label_com_docker_swarm_service_name))
					dockerServiceCmdOut, err := dockerServiceCmd.Output()

					//dockerServiceCmd, err := ioutil.ReadFile("docker_service_inspect.json") //execute command docker service inspect <<servicename>>

					if err != nil {
						panic(err)
					}

					var dockerServiceInspect []dockerServiceInspectOut
					err = json.Unmarshal(dockerServiceCmdOut, &dockerServiceInspect)

					if err != nil {
						fmt.Println(err)
					}

					if dockerServiceInspect[0].Spec.Labels.Swarm_swarmscale == "true" {

						fmt.Println(fmt.Sprintf("Swarmscale service %s", promResult.Metric.Container_label_com_docker_swarm_service_name))

						if dockerServiceInspect[0].Spec.Labels.Swarm_swarmscale_maximum == "nodes" {
							dockerServiceInspect[0].Spec.Labels.Swarm_swarmscale_maximum = string(nodeCount)
						}

						if  dockerServiceInspect[0].Spec.Mode.Replicated.Replicas < stringToInt(dockerServiceInspect[0].Spec.Labels.Swarm_swarmscale_minimum) {
							fmt.Println(fmt.Sprintf("Amount of replicas %d below minimum. Scaling service %s to %s replicas",dockerServiceInspect[0].Spec.Mode.Replicated.Replicas, promResult.Metric.Container_label_com_docker_swarm_service_name,dockerServiceInspect[0].Spec.Labels.Swarm_swarmscale_minimum))
							scaleCmd := exec.Command("/bin/sh", "-c", fmt.Sprintf("/usr/bin/docker service scale %s=%s", promResult.Metric.Container_label_com_docker_swarm_service_name,dockerServiceInspect[0].Spec.Labels.Swarm_swarmscale_minimum))

							scaleCmdOut, err := scaleCmd.Output()

							if err != nil {
								fmt.Println(err)
								panic(err)
							}

							fmt.Println(string(scaleCmdOut))
						}

						if  dockerServiceInspect[0].Spec.Mode.Replicated.Replicas > stringToInt(dockerServiceInspect[0].Spec.Labels.Swarm_swarmscale_maximum) {
							fmt.Println(fmt.Sprintf("Amount of replicas %d above maximum. Scaling service %s to %s replicas",dockerServiceInspect[0].Spec.Mode.Replicated.Replicas,promResult.Metric.Container_label_com_docker_swarm_service_name,dockerServiceInspect[0].Spec.Labels.Swarm_swarmscale_minimum))
							scaleCmd := exec.Command("/bin/sh", "-c", fmt.Sprintf("/usr/bin/docker service scale %s=%s", promResult.Metric.Container_label_com_docker_swarm_service_name,dockerServiceInspect[0].Spec.Labels.Swarm_swarmscale_maximum))

							scaleCmdOut, err := scaleCmd.Output()

							if err != nil {
								fmt.Println(err)
								panic(err)
							}

							fmt.Println(string(scaleCmdOut))
						}

						// do scale up here
						if stringToInt(promResult.Value[0]) > CPU_PERCENTAGE_UPPER_LIMIT {
							if dockerServiceInspect[0].Spec.Mode.Replicated.Replicas + 1 > stringToInt(dockerServiceInspect[0].Spec.Labels.Swarm_swarmscale_maximum) {
								fmt.Println(fmt.Sprintf("Service %s cannot be scaled above maximum of %s",promResult.Metric.Container_label_com_docker_swarm_service_name,dockerServiceInspect[0].Spec.Labels.Swarm_swarmscale_maximum))
							} else {
								fmt.Println(fmt.Sprintf("Scaling service %s to %d replicas",promResult.Metric.Container_label_com_docker_swarm_service_name,dockerServiceInspect[0].Spec.Mode.Replicated.Replicas + 1))
								scaleCmd := exec.Command("/bin/sh", "-c", fmt.Sprintf("/usr/bin/docker service scale %s=%d", promResult.Metric.Container_label_com_docker_swarm_service_name,dockerServiceInspect[0].Spec.Mode.Replicated.Replicas + 1))

								scaleCmdOut, err := scaleCmd.Output()

								if err != nil {
									fmt.Println(err)
									panic(err)
								}

								fmt.Println(string(scaleCmdOut))
							}
						}

						//do scale down here
						if stringToInt(promResult.Value[0]) < CPU_PERCENTAGE_LOWER_LIMIT {
							if dockerServiceInspect[0].Spec.Mode.Replicated.Replicas - 1 < stringToInt(dockerServiceInspect[0].Spec.Labels.Swarm_swarmscale_minimum) {
								fmt.Println(fmt.Sprintf("Service %s cannot be scaled below minumum of %s",promResult.Metric.Container_label_com_docker_swarm_service_name,dockerServiceInspect[0].Spec.Labels.Swarm_swarmscale_minimum))
							} else {
								fmt.Println(fmt.Sprintf("Scaling service %s to %d replicas",promResult.Metric.Container_label_com_docker_swarm_service_name,dockerServiceInspect[0].Spec.Mode.Replicated.Replicas - 1))
								scaleCmd := exec.Command("/bin/sh", "-c", fmt.Sprintf("/usr/bin/docker service scale %s=%d", promResult.Metric.Container_label_com_docker_swarm_service_name,dockerServiceInspect[0].Spec.Mode.Replicated.Replicas - 1))

								scaleCmdOut, err := scaleCmd.Output()

								if err != nil {
									fmt.Println(err)
									panic(err)
								}

								fmt.Println(string(scaleCmdOut))
							}
						}

					} else {
						fmt.Println(fmt.Sprintf("Service %s does not have swarmscale labels", dockerServiceInspect[0].Spec.Name))
						fmt.Println()
					}
				}
			}

			_ = res.Body.Close()
		}

		fmt.Println("--------------------------------------------------------")
		time.Sleep(30*time.Second)
	}
}

func stringToInt(in string) int {
	out, _ := strconv.Atoi(in)
	return out
}